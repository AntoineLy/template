## General description

The following package allows to process DESCRIBE YOUR PROJECT HERE.

==> **TODO general description of the package (description of folders and their usage)**



### Install virtual environment and dependencies

A requirement files is specified as well as spec-file to replicate the environment used to execute the code. To install environment with conda:

    conda install --name YOURPROJECTENV --file spec-file.txt

Otherwise, you can use the command :

    conda env crate -n YOURPROJECTENV python=3.6.8


Once the environment is created, active it:

  conda activate YOURPROJECTENV

And install requirements:

    pip install -r requirements.txt

The different files to replicate envrionment have been generated with following commands:

    conda activate YOURPROJECTENV
    pip freeze > requirements.txt
    conda list --explicit > spec-file.txt
    conda env export > environment.yml

for Linux `conda activate` might be replaced by `source activate`. Be careful, the three files (requirements, spec-file and environment) have to be used to replicate the environment. Indeed, even if it is not recommanded, some libraries have to be installed with `pip` and others with `conda`.

## Launch unit tests

Point cursor to the module folder then run

    cd ./scripts
    py.test --cov-report term-missing --cov=myScripts -v

## Produce the documentation

### Convention used for code documentation

Sphinx can generate documentation automatically by screening the code. To make it works, user has to respect a specific syntax when coding. For example, the convention below can be respected to implement functions or class.

        """Short summary.

        Parameters
        ----------
        x : type
            Description of parameter `x`.

        Returns
        -------
        type
            Description of returned object.

        """
        return 2

### To generate Documentation with Sphinx

The package contains a .bat file that allows the production of full html documentation.

To generate the HTML documentation please follow instructions below

    cd $PATH/documentation
    make html

or alternatively use

    sphinx-build -b html source build

To generate Latex Documentation

    sphinx-build -b latex source latex

The previous command will translate the documentation into a .tex file you can then compile with Latex


### Use Sphinx for the first time

To install Sphinx, please refer to this [link](http://www.sphinx-doc.org/en/master/usage/installation.html).
Once installed, to init a documentation folder run the following command with terminal (cursor has to point to appropriate directory)

    sphinx-quickstart
