#init of unittests folder cf Readme.md of repo root to run it with pytest

import sys
import os


sys.path.insert(1, os.path.abspath('.'))
sys.path.insert(2, os.path.abspath('..'))
sys.path.insert(3, os.path.abspath('../..'))
sys.setrecursionlimit(1500)
print(sys.path)
