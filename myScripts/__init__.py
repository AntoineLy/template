"""
Module to process Sentiment Index
=============================================

.. moduleauthor:: Antoine Ly <antoine.ly@invalid.com>

"""

import sys
import os


sys.path.insert(1, os.path.abspath('.'))
sys.path.insert(2, os.path.abspath('..'))
sys.path.insert(3, os.path.abspath('../..'))
sys.setrecursionlimit(1500)
from .myConfig import *
print("myScript __init__")
