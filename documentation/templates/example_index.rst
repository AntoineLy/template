.. Sentiment Index Politique EN documentation master file, created by
   sphinx-quickstart on Thu Feb 14 15:58:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Introduction
==========================================

The purpose of this documentation is to explain in details the motivation of the developed library as well as to provide insights on implemented code. The package can be considered as well as an example of best practices to develop in python.

.. note::
   This documentation is dedicated to anyone who are discovering this library. It is not dissociable of any files associated to the repository that contains this document.



.. toctree::
   dependencies.rst
   autodoc.rst
   outputsGeneration.rst
   :maxdepth: 5
   :caption: Contents:







Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
