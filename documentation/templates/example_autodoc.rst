API Documentation
==================

The current modules is split into 3 main sections.
The first section is dedicated to scripts developed for the `Sentiment Index project`. The module is quite generic as soon as it is used for text-mining.
Generic functions are located into the `Generic modules` section. Finally, the last module list every function which has been unittested. User can use the `source` link to overview scripts.

.. automodule:: myScripts


Specific to SI Project
-----------------------

.. automodule:: myScripts.main
   :members:
.. automodule:: myScripts.loading_data
   :members:
.. automodule:: myScripts.preprocessing
   :members:
.. automodule:: myScripts.textProcessor
   :members:
.. automodule:: myScripts.analyzer
   :members:

.. autoclass:: myScripts.textProcessor.textProcessor
   :members:

   .. automethod:: __init__


Generic modules
---------------
.. automodule:: myScripts.myUtils
   :members:
.. automodule:: myScripts.myExceptions
   :members:
.. automodule:: myScripts.myConfig
   :members:

Unittests modules
-----------------
.. automodule:: myScripts.unittests.test_loading_data
   :members:
.. automodule:: myScripts.unittests.test_preprocessing
   :members:
.. automodule:: myScripts.unittests.test_textProcessor
   :members:
.. automodule:: myScripts.unittests.test_myConfig
   :members:
.. automodule:: myScripts.unittests.test_myUtils
  :members:
.. automodule:: myScripts.unittests.test_myExceptions
   :members:
